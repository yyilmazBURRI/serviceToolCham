import React, { useEffect, useState } from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { makeStyles, ServerStyleSheets } from "@material-ui/core/styles";
import firebase from "firebase";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Logo from "../../../assets/logo.png";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";

import Collapse from "@material-ui/core/Collapse";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Box from "@material-ui/core/Box";
import Rating from "@material-ui/lab/Rating";
import moment from "moment";
import ReactMapGL, { Marker } from "react-map-gl";
import { Drawer, List, ListItem, ListItemText } from "@material-ui/core";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import InputLabel from "@material-ui/core/InputLabel";
import { useCookies } from "react-cookie";

import LoadingOverlay from "react-loading-overlay";
import { delay } from "q";
import Cham from "../../../assets/cham.jpg";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },

  menuButton: {
    marginRight: theme.spacing(2),
  },

  paper: {
    margin: 15,
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },

  imageHolder: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    width: "100%",
  },

  buttonLeft: {
    textAlign: "center",
    display: "block",
    width: "auto",
    border: "1px solid #000000",
    borderRadius: 0,
    padding: 16,
    paddingTop: 8,
    paddingBottom: 8,
    fontWeight: "bold",
    fontSize: 12,
    textTransform: "uppercase",
    backgroundColor: "black",
    color: "white",
    fill: "white",
    marginTop: 5,
  },

  button: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    width: "auto",
    border: "1px solid #000000",
    borderRadius: 0,
    padding: 16,
    paddingTop: 8,
    paddingBottom: 8,
    fontWeight: "bold",
    fontSize: 12,
    textTransform: "uppercase",
    backgroundColor: "black",
    color: "white",
    fill: "white",
  },

  uiinput: {
    backgroundColor: "#EDEDED",
    border: "1px solid #D9D9D9",
    borderRadius: 0,
    padding: 4,
    paddingLeft: 8,
    paddingRight: 8,
    cursor: "text",
    fontWeight: "bold",
    fontSize: 16,
    width: 230,
    maxHeight: 28,
    margin: 5,
    textDecoration: "none",
    transitionProperty: "color, border-color",
    transitionDuration: 0.1,
    transitionTimingFunction: "ease-in-out",
    boxShadow: "none",
  },

  uitable: {
    width: "100%",
    borderCollapse: "collapse",
    fontWeight: "normal",
    fontStyle: "normal",
    color: "#404040",
    margin: "0px 0px 0px 0px",
    padding: "0px 0px 0px 0px",
    cursor: "default",
    boxSizing: "border-box",
  },

  input: {
    width: 0.1,
    height: 0.1,
    opacity: 0,
    overflow: "hidden",
    position: "absolute",
    zIndex: -1,
  },

  celllabel: {
    borderTop: "none",
    color: "rgb(0,120,191)",
    wordBreak: "keep-all",
    textWrap: "avoid",
    whiteSpace: "nowrap",
    textTransform: "uppercase",
    fontSize: 11,
    fontWeight: "bold",
  },
  selectBox: {
    backgroundColor: "#EDEDED",
    borderRadius: 2.5,
    padding: 15,
    fontFamily: "'Open Sans', sans-serif",
    transition: "all 0.1s ease-in-out",
    "&:hover": {
      borderLeft: "10px black solid",
    },
  },
}));

const labels = {
  0.5: "Useless",
  1: "Useless+",
  1.5: "Poor",
  2: "Poor+",
  2.5: "Ok",
  3: "Ok+",
  3.5: "Good",
  4: "Good+",
  4.5: "Excellent",
  5: "Excellent+",
};

const PublicView = (props) => {
  const objectid = props.match.params.objectid;
  const [viewport, setViewport] = useState({
    width: "100%",
    height: 400,
    latitude: 47.17829041524882,
    longitude: 8.46158514871517,
    zoom: 15,
  });

  const [images, setImages] = useState([]);
  const [serviceImages, setServiceImages] = useState([]);
  const [checked, setChecked] = useState([]);
  const [isDone, setIsDone] = useState([]);
  const [isOpen, setIsOpen] = useState(false);
  const [loginOpen, setLoginOpen] = useState(false);
  const [hover, setHover] = useState(-1);
  const [object, setObject] = useState({});
  const [Ratingvalue, setRatingvalue] = useState(0);
  const [Meldung, setMeldung] = useState("");
  const [open, setOpen] = useState(false);
  const [ratings, setRatings] = useState([]);
  const [benutzername, setBenutzername] = useState("");
  const [passwort, setPasswort] = useState("");
  const [cookies, setCookie] = useCookies(["user"]);
  const [incidents, setIncidents] = useState([]);
  const classes = useStyles();
  const [services, setServices] = useState([]);
  const [isDoneServices, setIsDoneServices] = useState([]);
  const [isCheckedServices, setIsCheckedServices] = useState([]);
  const [Longitude, setLongitude] = useState(0);
  const [Latitude, setLatitude] = useState(0);
  const [loading, setLoading] = useState(true);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = () => {
    const push = firebase
      .database()
      .ref("/Incidents")
      .push({
        ObjectID: object.RecID,
        Meldung: Meldung,
        Status: "Offen",
        CreateDate: moment().format("DD-MM-YYYY"),
        Longitude: object.Longitude,
        Latitude: object.Latitude,
      })
      .then((push) => {
        firebase
          .database()
          .ref("serviceobjekte/" + objectid + "/incidents/" + push.getKey())
          .update({
            IncidentID: push.getKey(),
            Meldung: Meldung,
            Status: "Offen",
            CreateDate: moment().format("DD-MM-YYYY"),
            Longitude: object.Longitude,
            Latitude: object.Latitude,
          });
      });

    setOpen(false);
  };

  useEffect(() => {
    firebase
      .database()
      .ref("/serviceobjekte/" + objectid)
      .on("value", (snapshot) => {
        setObject(snapshot.val());
        setLatitude(snapshot.val().Latitude);
        setLongitude(snapshot.val().Longitude);
        delay(500).then(() => setLoading(false));
        if (snapshot.val().Zustand) {
          let ratings = [];
          const ratingsValue = snapshot.val().Zustand;
          const keys = Object.keys(ratingsValue);
          for (var i = 0; i < keys.length; i++) {
            var k = keys[i];
            var Rating = ratingsValue[k].Rating;
            ratings.push(Rating);
          }
          setRatings(ratings);
        }
      });
    firebase
      .database()
      .ref("/serviceobjekte/" + objectid + "/incidents")
      .on("value", (snapshot) => {
        if (snapshot.val() !== null) {
          let incidents = [];
          const incidentsvalue = snapshot.val();
          const keys = Object.keys(incidentsvalue);
          for (var i = 0; i < keys.length; i++) {
            var k = keys[i];
            if (incidentsvalue[k].Status === "Offen") {
              var IncidentID = incidentsvalue[k].IncidentID;
              var CreateDate = incidentsvalue[k].CreateDate;
              var Meldung = incidentsvalue[k].Meldung;
              var Status = incidentsvalue[k].Status;
              incidents.push({ CreateDate, Meldung, Status, IncidentID });
            }
          }
          setIncidents(incidents);
          setChecked(incidents.map((incident, index) => false));
          setIsDone(incidents.map((incident, index) => false));
        }
      });
    firebase
      .database()
      .ref("/serviceobjekte/" + objectid + "/service")
      .on("value", (snapshot) => {
        if (snapshot.val() !== null) {
          let services = [];
          const servicevalue = snapshot.val();
          const keys = Object.keys(servicevalue);
          for (var i = 0; i < keys.length; i++) {
            var k = keys[i];
            if (servicevalue[k].Status === "Offen") {
              var wartungID = servicevalue[k].wartungID;
              var serviceDatum = servicevalue[k].serviceDatum;
              var Bemerkungen = servicevalue[k].Bemerkungen;
              var Task = servicevalue[k].Task;
              services.push({ wartungID, serviceDatum, Bemerkungen, Task });
            }
          }
          setServices(services);
          setIsCheckedServices(services.map(() => false));
          setIsDoneServices(services.map(() => false));
        }
      });
  }, []);

  const onChange = (imageList, addUpdateIndex) => {
    setImages(imageList);
  };

  const handleLoginSubmit = () => {
    setCookie("user", "Max Müller", { path: "/" });
    setLoginOpen(false);
  };

  const handleServiceImageDelete = (index) => {
    let serviceImageHolder = serviceImages.slice();
    serviceImageHolder.splice(index, 1);
    setServiceImages(serviceImageHolder);
  };

  const handleImageDelete = (index) => {
    let imageHolder = images.slice();
    imageHolder.splice(index, 1);
    setImages(imageHolder);
  };

  return (
    <>
      <AppBar
        position="sticky"
        style={{ backgroundColor: "rgb(64, 64, 64)", width: "100%" }}
      >
        <Toolbar>
          <IconButton
            onClick={() => setIsOpen(true)}
            edge="end"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
            <MenuIcon />
          </IconButton>
          <img
            src={Logo}
            alt="logo"
            style={{ height: 30, position: "absolute", right: 15 }}
          />
        </Toolbar>
      </AppBar>
      {cookies.user === "Max Müller" ? (
        <>
          <Paper elevation={0} style={{ marginTop: 40, padding: 40 }}>
            <h2>Offene Incidents</h2>
            {incidents.map((incident, index) => (
              <>
                <div
                  className={classes.selectBox}
                  onClick={() => {
                    if (checked[index] === true) {
                      const arrayCopy = checked.slice();
                      arrayCopy[index] = false;
                      setChecked(arrayCopy);
                    } else {
                      const arrayCopy = checked.slice();
                      arrayCopy[index] = true;
                      setChecked(arrayCopy);
                    }
                  }}
                >
                  {`${index + 1}. ${incident.CreateDate} - ${incident.Meldung}`}
                </div>
                <Collapse
                  in={checked[index]}
                  style={{ border: "1px solid #EDEDED", padding: 5 }}
                >
                  <span>{`Datum der Meldung: ${incident.CreateDate}`} </span>
                  <br />
                  <span>{`Meldungstext: ${incident.Meldung}`} </span>
                  <br />
                  <span>Ist das Problem beseitigt? </span>
                  <input
                    type="checkbox"
                    id="solved"
                    name="solved"
                    onClick={() => {
                      if (isDone[index] === true) {
                        const arrayCopy = isDone.slice();
                        arrayCopy[index] = false;
                        setIsDone(arrayCopy);
                      } else {
                        const arrayCopy = isDone.slice();
                        arrayCopy[index] = true;
                        setIsDone(arrayCopy);
                      }
                    }}
                  />
                  <br />
                  <span />
                  <span>
                    <div className="upload__image-wrapper">
                      {images[index] === undefined ? (
                        <div>
                          <input
                            id={`image${index}`}
                            // label="Bild hinzufügen"
                            capture="camera"
                            type="file"
                            className={classes.input}
                            onChange={(file) =>
                              setImages([
                                ...images,
                                {
                                  File: file.target.files[0],
                                  incidentIndex: index,
                                },
                              ])
                            }
                          />
                          <label
                            for={`image${index}`}
                            className={classes.buttonLeft}
                          >
                            Datei hochladen
                          </label>
                        </div>
                      ) : (
                        <div key={index} className="image-item">
                          <img
                            src={URL.createObjectURL(images[index].File)}
                            alt=""
                            width="100"
                          />
                          <div className="image-item__btn-wrapper">
                            <button
                              className={classes.buttonLeft}
                              onClick={() => handleImageDelete(index)}
                            >
                              Bild löschen
                            </button>
                          </div>
                        </div>
                      )}
                    </div>
                    <button
                      className={classes.buttonLeft}
                      style={{ marginLeft: 0 }}
                      onClick={() =>
                        firebase
                          .database()
                          .ref(
                            "/serviceobjekte/" +
                              objectid +
                              "/incidents/" +
                              incident.IncidentID
                          )
                          .update({
                            Status: "Abgeschlossen",
                            MA: "Max Müller",
                            DatumErledigt: moment().format("DD-MM-YYYY"),
                          })
                          .then(() => {
                            firebase
                              .database()
                              .ref("Incidents/" + incident.IncidentID)
                              .update({
                                Status: "Abgeschlossen",
                                MA: "Max Müller",
                                DatumErledigt: moment().format("DD-MM-YYYY"),
                              })
                              .then(() => {
                                return images.length > 0
                                  ? firebase
                                      .storage()
                                      .ref()
                                      .child(
                                        "/incidentphotos/" + incident.IncidentID
                                      )
                                      .put(images[index].File)
                                      .then((snapshot) => {
                                        snapshot.ref
                                          .getDownloadURL()
                                          .then((downloadURL) => {
                                            firebase
                                              .database()
                                              .ref(
                                                "/serviceobjekte/" +
                                                  objectid +
                                                  "/incidents/" +
                                                  incident.IncidentID
                                              )
                                              .update({
                                                Produktbild: downloadURL,
                                              })
                                              .then(() => {
                                                firebase
                                                  .database()
                                                  .ref(
                                                    "Incidents/" +
                                                      incident.IncidentID
                                                  )
                                                  .update({
                                                    Produktbild: downloadURL,
                                                  });
                                              });
                                          });
                                      })
                                  : null;
                              });
                          })
                      }
                    >
                      Fertigstellen
                    </button>
                  </span>
                  <br />
                </Collapse>
              </>
            ))}
          </Paper>
          <Paper elevation={0} style={{ marginTop: 40, padding: 40 }}>
            <h2>Offene Wartungsarbeiten</h2>
            {services.map((service, index) => (
              <>
                <div
                  className={classes.selectBox}
                  onClick={() => {
                    if (isCheckedServices[index] === true) {
                      const arrayCopy = isCheckedServices.slice();
                      arrayCopy[index] = false;
                      setIsCheckedServices(arrayCopy);
                    } else {
                      const arrayCopy = isCheckedServices.slice();
                      arrayCopy[index] = true;
                      setIsCheckedServices(arrayCopy);
                    }
                  }}
                >
                  {`${index + 1}. ${service.serviceDatum} - ${service.Task}`}
                </div>
                <Collapse in={isCheckedServices[index]}>
                  <span>{`Datum des Services: ${service.serviceDatum}`} </span>
                  <br />
                  <span>{`Bemerkung: ${service.Bemerkungen}`} </span>
                  <br />
                  <span>Ist der Service erledigt? </span>
                  <input
                    type="checkbox"
                    id="solved"
                    name="solved"
                    onClick={() => {
                      if (isDoneServices[index] === true) {
                        const arrayCopy = isDoneServices.slice();
                        arrayCopy[index] = false;
                        setIsDoneServices(arrayCopy);
                      } else {
                        const arrayCopy = isDoneServices.slice();
                        arrayCopy[index] = true;
                        setIsDoneServices(arrayCopy);
                      }
                    }}
                  />
                  <br />
                  <span />
                  <span>
                    <div className="upload__image-wrapper">
                      {serviceImages[index] === undefined ? (
                        <div>
                          <input
                            id={`serviceBild${index}`}
                            // label="Bild hinzufügen"
                            capture="camera"
                            type="file"
                            className={classes.input}
                            onChange={(file) =>
                              setServiceImages([
                                ...serviceImages,
                                {
                                  File: file.target.files[0],
                                  serviceIndex: index,
                                },
                              ])
                            }
                          />
                          <label
                            for={`serviceBild${index}`}
                            className={classes.buttonLeft}
                          >
                            Datei hochladen
                          </label>
                        </div>
                      ) : (
                        <div key={index} className="image-item">
                          <img
                            src={URL.createObjectURL(serviceImages[index].File)}
                            alt=""
                            width="100"
                          />
                          <div className="image-item__btn-wrapper">
                            <button
                              className={classes.buttonLeft}
                              onClick={() => handleServiceImageDelete(index)}
                            >
                              Bild löschen
                            </button>
                          </div>
                        </div>
                      )}
                    </div>
                    <button
                      className={classes.buttonLeft}
                      onClick={() =>
                        firebase
                          .database()
                          .ref(
                            "/serviceobjekte/" +
                              objectid +
                              "/service/" +
                              service.wartungID
                          )
                          .update({
                            Status: "Abgeschlossen",
                            MA: "Max Müller",
                            DatumErledigt: moment().format("DD-MM-YYYY"),
                          })
                          .then(() => {
                            firebase
                              .database()
                              .ref("wartungen/" + service.wartungID)
                              .update({
                                Status: "Abgeschlossen",
                                MA: "Max Müller",
                                DatumErledigt: moment().format("DD-MM-YYYY"),
                              })
                              .then(() => {
                                return serviceImages.length > 0
                                  ? firebase
                                      .storage()
                                      .ref()
                                      .child(
                                        "/servicephotos/" + service.wartungID
                                      )
                                      .put(serviceImages[index].File)
                                      .then((snapshot) => {
                                        snapshot.ref
                                          .getDownloadURL()
                                          .then((downloadURL) => {
                                            firebase
                                              .database()
                                              .ref(
                                                "/serviceobjekte/" +
                                                  objectid +
                                                  "/service/" +
                                                  service.wartungID
                                              )
                                              .update({
                                                Servicebild: downloadURL,
                                              })
                                              .then(() => {
                                                firebase
                                                  .database()
                                                  .ref(
                                                    "wartungen/" +
                                                      service.wartungID
                                                  )
                                                  .update({
                                                    Servicebild: downloadURL,
                                                  })
                                                  .then(() => {
                                                    setServiceImages([]);
                                                  });
                                              });
                                          });
                                      })
                                  : null;
                              });
                          })
                      }
                    >
                      Fertigstellen
                    </button>
                  </span>
                  <br />
                </Collapse>
              </>
            ))}
          </Paper>
          <Paper elevation={0} style={{ marginTop: 40, padding: 40 }}>
            <h2>Objektposition</h2>
            <button
              className={classes.button}
              onClick={() => {
                const geo = navigator.geolocation;
                const success = (pos) => {
                  firebase
                    .database()
                    .ref("serviceobjekte/" + objectid)
                    .update({
                      Latitude: pos.coords.latitude,
                      Longitude: pos.coords.longitude,
                    });
                };

                geo.getCurrentPosition(success);
              }}
            >
              Objektstandort neu erfassen
            </button>
          </Paper>
        </>
      ) : (
        <>
          <LoadingOverlay
            styles={{
              overlay: (base) => ({
                ...base,
                background: "rgb(64, 64, 64)",
                height: "calc(100vh - 56px)",
                zIndex: 9999,
              }),
              content: (base) => ({
                ...base,
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
              }),
            }}
            active={loading}
            spinner
            text="Bitte kurz warten..."
          />
          {loading ? null : (
            <Grid container direction={"column"} spacing={0}>
              <Grid item xs={12}></Grid>
              <Grid item xs={12}>
                <h1
                  style={{
                    position: "absolute",
                    top: "35vh",
                    color: "white",
                    opacity: 1,
                    marginTop: 0,
                    padding: 16,
                    textAlign: "center",
                    zIndex: 999,
                  }}
                >
                  Herzlich willkommen in der Gemeinde Cham
                </h1>
                <div
                  style={{
                    height: "calc(100vh - 56px)",
                    backgroundImage: `url(${Cham})`,
                    backgroundSize: "cover",
                    backgroundRepeat: "no-repeat",
                  }}
                >
                  <div
                    style={{
                      background: "black",
                      height: "100%",
                      opacity: 0.6,
                    }}
                  ></div>
                </div>
                <Paper elevation={0} style={{ marginTop: 40, padding: 40 }}>
                  <h2>Burri {object.Produkt}</h2>
                  <img
                    src={object.Produktbild}
                    className={classes.imageHolder}
                  />
                  <p>
                    Die Landi Bank gehört in der Schweiz fest zum Stadtbild.
                    Entworfen für die Schweizer Landesausstellung 1939, leistet
                    die Landi Bank von BURRI seit Jahrzehnten ihren Dienst in
                    öffentlichen Parkanlagen, auf Stadtplätzen, an
                    Aussichtspunkten und auf Schweizer Bahnhöfen. Langlebigkeit
                    und eine zeitlose Form machen «die Landi» zu einer bei allen
                    Generationen beliebten Sitzbank im öffentlichen Raum.
                  </p>
                </Paper>
              </Grid>
              <Grid item xs={12}>
                <Paper elevation={0} style={{ marginTop: 40, padding: 40 }}>
                  <h2>Wie bewerten Sie den Zustand dieses Objekts?</h2>
                  <Rating
                    size="large"
                    name="hover-feedback"
                    value={Ratingvalue}
                    disabled={Ratingvalue !== 0 ? true : false}
                    onChange={(event, newValue) => {
                      setRatingvalue(newValue);
                      const push = firebase
                        .database()
                        .ref("/Zustand/" + objectid)
                        .push({
                          ObjectID: objectid,
                          Rating: newValue,
                          CreateDate: moment().format("DD-MM-YYYY"),
                        })
                        .then(() => {
                          firebase
                            .database()
                            .ref("/serviceobjekte/" + objectid + "/Zustand")
                            .push({
                              Rating: newValue,
                            });
                        });
                    }}
                    onChangeActive={(event, newHover) => {
                      setHover(newHover);
                    }}
                  />
                  <h3>
                    Durchschnittliche Bewertung:{" "}
                    {ratings.length == 0
                      ? 0
                      : (
                          ratings.reduce((acc, v) => acc + v) / ratings.length
                        ).toFixed(1)}
                  </h3>
                </Paper>
              </Grid>
              <Grid item xs={12}>
                <Paper elevation={0} style={{ marginTop: 40, padding: 40 }}>
                  <h2>Möchten Sie eine Beschädigung melden?</h2>
                  <button
                    style={{
                      display: "block",
                      marginLeft: "auto",
                      marginRight: "auto",
                      width: "40%",
                      border: "1px solid #000000",
                      borderRadius: 0,
                      padding: 16,
                      paddingTop: 8,
                      paddingBottom: 8,
                      fontWeight: "bold",
                      fontSize: 12,
                      textTransform: "uppercase",
                      backgroundColor: "black",
                      color: "white",
                      fill: "white",
                    }}
                    onClick={handleClickOpen}
                  >
                    Meldung einreichen
                  </button>
                </Paper>
              </Grid>
              <Grid item xs={12}>
                <Paper
                  elevation={0}
                  style={{ marginTop: 40, paddingBottom: 40, paddingTop: 40 }}
                >
                  <h2 style={{ paddingLeft: 40 }}>Wo bin ich?</h2>
                  <ReactMapGL
                    mapboxApiAccessToken={
                      "pk.eyJ1IjoiYnVycmkiLCJhIjoiY2tuc2loMWs4MmkydzJxbnhidmFkYmRtYyJ9.OM79ZdZ8JN6JY7j30TQ9aw"
                    }
                    {...viewport}
                    onViewportChange={(nextViewport) =>
                      setViewport(nextViewport)
                    }
                    mapStyle={"mapbox://styles/mapbox/basic-v9"}
                  >
                    <Marker latitude={Latitude} longitude={Longitude}>
                      <div>
                        <LocationOnIcon /> {"Mein Standort"}
                      </div>
                    </Marker>
                  </ReactMapGL>
                </Paper>
              </Grid>
              <Grid item xs={12}>
                <Paper elevation={0} style={{ marginTop: 40, padding: 40 }}>
                  <h2>Pflege und Unterhalt</h2>
                  <p>
                    Ihr Werkdienst pflegt die public elements® regelmässig und
                    ermöglicht somit einen attraktiven und sicheren öffentlichen
                    Raum.{" "}
                  </p>
                  <table className={classes.uitable}>
                    <thead>
                      <tr className={classes.celllabel}>
                        <th>Datum</th>
                        <th>Service-Arbeiter</th>
                        <th>Aktion</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>21.7.2021</td>
                        <td>Max Müller</td>
                        <td>Check + Reinigung</td>
                      </tr>
                      <tr>
                        <td>26.7.2021</td>
                        <td>Heiri Meier</td>
                        <td>Check + Reinigung</td>
                      </tr>
                    </tbody>
                  </table>
                </Paper>
              </Grid>
            </Grid>
          )}
        </>
      )}
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Meldung einreichen</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Bitte beschreiben Sie im unterem Textfeld das Problem mit dem
            aktuellem Objekt
          </DialogContentText>
          <textarea
            className={classes.uiinput}
            value={Meldung}
            onChange={(e) => setMeldung(e.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Abbrechen
          </Button>
          <Button onClick={handleSubmit} color="primary">
            Senden
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={loginOpen}
        onClose={() => setLoginOpen(false)}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Anmelden</DialogTitle>
        <DialogContent>
          <InputLabel
            htmlFor="Artikelnummer-simple"
            className={classes.uilabel}
          >
            Benutzername*
          </InputLabel>
          <input
            className={classes.uiinput}
            required
            value={benutzername}
            onChange={(e) => setBenutzername(e.target.value)}
          />
          <InputLabel
            htmlFor="Artikelnummer-simple"
            className={classes.uilabel}
          >
            Password*
          </InputLabel>
          <input
            className={classes.uiinput}
            required
            value={passwort}
            onChange={(e) => setPasswort(e.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setLoginOpen(false)} color="primary">
            Abbrechen
          </Button>
          <Button onClick={() => handleLoginSubmit()} color="primary">
            Anmelden
          </Button>
        </DialogActions>
      </Dialog>
      <Drawer anchor="left" open={isOpen} onClose={() => setIsOpen(false)}>
        <List style={{ minWidth: 150 }}>
          {cookies.user === "Max Müller" ? (
            <ListItem
              button
              text={"Abmelden"}
              onClick={() => setCookie("user", null, { path: "/" })}
            >
              <ListItemText>Abmelden</ListItemText>
            </ListItem>
          ) : (
            <ListItem
              button
              text={"Anmelden"}
              onClick={() => setLoginOpen(true)}
            >
              <ListItemText>Anmelden</ListItemText>
            </ListItem>
          )}
        </List>
      </Drawer>
    </>
  );
};

export default PublicView;
