import React, { useEffect, useState, forwardRef } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import { Bar } from "react-chartjs-2";
import firebase from "firebase";
import MaterialTable from "material-table";
import { Doughnut } from "react-chartjs-2";
import moment from "moment";

import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

import AddBox from "@material-ui/icons/AddBox";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";
import AddWartung from "../../components/addWartung";
import WartungMap from "../../components/wartungMap";

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    height: "100%",
    "& > *": {
      marginLeft: theme.spacing(5),
      marginTop: theme.spacing(5),
      padding: theme.spacing(4),
      width: "100%",
    },
  },
}));

const AdminWartung = () => {
  const [wartungen, setWartungen] = useState([]);
  const [objects, setObjects] = useState([]);

  const [open, setOpen] = useState(false);

  const classes = useStyles();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const countOccurrencesOf = (search) => {
    return wartungen.filter((el) => el.Status.includes(search)).length;
  };

  const data = {
    labels: ["Offen", "In Bearbeitung", "Abgeschlossen"],
    datasets: [
      {
        label: "# of Votes",
        data: [
          countOccurrencesOf("Offen"),
          countOccurrencesOf("In Bearbeitung"),
          countOccurrencesOf("Abgeschlossen"),
        ],
        backgroundColor: [
          "rgba(255, 99, 132, 0.2)",
          "rgba(54, 162, 235, 0.2)",
          "rgba(255, 206, 86, 0.2)",
        ],
        borderColor: [
          "rgba(255, 99, 132, 1)",
          "rgba(54, 162, 235, 1)",
          "rgba(255, 206, 86, 1)",
        ],
        borderWidth: 1,
      },
    ],
  };

  useEffect(() => {
    firebase
      .database()
      .ref("/wartungen")
      .on("value", (snapshot) => {
        if (snapshot.val()) {
          let wartungen = [];
          const wartungenValue = snapshot.val();
          const keys = Object.keys(wartungenValue);
          for (var i = 0; i < keys.length; i++) {
            var k = keys[i];
            var RecID = wartungenValue[k].RecID;
            var ObjektID = wartungenValue[k].ObjectID;
            var Objekt = wartungenValue[k].Objekt;
            var Adresse = wartungenValue[k].Adresse;
            var Zustand = wartungenValue[k].Zustand;
            var Task = wartungenValue[k].Task;
            var Bemerkungen = wartungenValue[k].Bemerkungen;
            var Status = wartungenValue[k].Status;
            var serviceDatum = wartungenValue[k].serviceDatum;
            var Longitude = wartungenValue[k].Longitude;
            var Latitude = wartungenValue[k].Latitude;
            wartungen.push({
              RecID,
              ObjektID,
              Objekt,
              Adresse,
              Task,
              Bemerkungen,
              Zustand,
              Status,
              serviceDatum,
              Longitude,
              Latitude,
            });
          }
          setWartungen(wartungen);
        }
      });
    firebase
      .database()
      .ref("/serviceobjekte")
      .on("value", (snapshot) => {
        if (snapshot.val()) {
          let objects = [];
          const objectsValue = snapshot.val();
          const keys = Object.keys(objectsValue);
          for (var i = 0; i < keys.length; i++) {
            var k = keys[i];
            var Adresse = objectsValue[k].Adresse;
            var Active = objectsValue[k].Active;
            var CreateDate = objectsValue[k].CreateDate;
            var CreateUser = objectsValue[k].CreateUser;
            var Kaufdatum = objectsValue[k].Kaufdatum;
            var Latitude = objectsValue[k].Latitude;
            var Longitude = objectsValue[k].Longitude;
            var ObjectID = objectsValue[k].ObjectID;
            var Produkt = objectsValue[k].Produkt;
            var Produktbild = objectsValue[k].Produktbild;
            var RecID = objectsValue[k].RecID;
            objects.push({
              Adresse,
              Active,
              CreateDate,
              CreateUser,
              Kaufdatum,
              Latitude,
              Longitude,
              ObjectID,
              Produkt,
              Produktbild,
              RecID,
            });
          }
          setObjects(objects);
        }
      });
  }, []);

  return (
    <>
      <Grid
        container
        direction={"row"}
        style={{ height: "auto", minHeight: "40%", paddingRight: 40 }}
      >
        <Grid item xs={7} style={{ height: "auto" }}>
          <div className={classes.root}>
            <Paper elevation={1}>
              <h1 style={{ marginTop: 0 }}>Standorte</h1>
              <WartungMap wartungen={wartungen} />
            </Paper>
          </div>
        </Grid>
        <Grid item xs={5} style={{ height: "80%" }}>
          <div className={classes.root}>
            <Paper elevation={1}>
              <>
                <h1 style={{ marginTop: 0 }}>Kategorie</h1>
                <Doughnut data={data} />
              </>
            </Paper>
          </div>
        </Grid>
      </Grid>
      <Grid
        container
        direction={"row"}
        style={{ paddingRight: 40, paddingBottom: 40 }}
      >
        <Grid item xs={12}>
          <div className={classes.root}>
            <MaterialTable
              title={""}
              icons={tableIcons}
              options={{
                filtering: true,
                columnsButton: true,
                headerStyle: {
                  fontFamily: "'Open Sans', sans-serif",
                  backgroundColor: "#ddd",
                  fontWeight: 600,
                  textAlign: "left",
                },
              }}
              columns={[
                { title: "Objekt-ID", field: "ObjektID" },
                {
                  title: "Gepl. Datum",
                  field: "serviceDatum",
                  cellStyle: (e, rowData) => {
                    if (
                      rowData.serviceDatum !== undefined &&
                      Date.parse(rowData.serviceDatum) <
                        Date.parse(moment().format("DD-MM-YYYY"))
                    ) {
                      return {
                        backgroundColor: "#FF4136",
                        fontWeight: 900,
                        color: "white",
                        minWidth: 129.75,
                      };
                    }
                    return { width: 129.75 };
                  },
                },
                { title: "Adresse", field: "Adresse" },
                { title: "Bemerkungen", field: "Bemerkungen" },
                { title: "Zustand", field: "Zustand" },
                { title: "Task", field: "Task" },
                { title: "Status", field: "Status" },
              ]}
              data={wartungen}
              actions={[
                {
                  icon: () => <AddBox />,
                  tooltip: "Bestellung hinzufügen",
                  isFreeAction: true,
                  onClick: () => setOpen(true),
                },
              ]}
            />
          </div>
        </Grid>
      </Grid>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle
          id="alert-dialog-title"
          style={{ paddingTop: 0, paddingBottom: 0 }}
        >
          <h1>Wartung erfassen</h1>
        </DialogTitle>
        <DialogContent>
          <AddWartung
            isFirstObject={wartungen.length === 0 ? 1 : wartungen.length + 1}
            objects={objects}
            handleClose={handleClose}
          />
        </DialogContent>
      </Dialog>
    </>
  );
};

export default AdminWartung;
