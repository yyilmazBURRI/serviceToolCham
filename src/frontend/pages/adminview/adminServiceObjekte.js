import React, { useEffect, useState, forwardRef } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import { Bar } from "react-chartjs-2";
import firebase from "firebase";
import MaterialTable from "material-table";
import { Doughnut } from "react-chartjs-2";

import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

import AddObjectTable from "../../components/addObject";

import AddBox from "@material-ui/icons/AddBox";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";
import ServiceObjekteMap from "../../components/serviceObjekteMap";

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    height: "100%",
    "& > *": {
      marginLeft: theme.spacing(5),
      marginTop: theme.spacing(5),
      padding: theme.spacing(4),
      width: "100%",
    },
  },
}));

const AdminServiceObjekte = () => {
  const [objects, setObjects] = useState([]);
  const [open, setOpen] = useState(false);

  const [incidentsCounter, setIncidentsCounter] = useState([]);
  const classes = useStyles();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const countOccurrencesOf = (search) => {
    return objects.filter((el) => el.Produkt.includes(search)).length;
  };

  const data = {
    labels: ["Bank", "Haltestelle", "Leuchte"],
    datasets: [
      {
        data: [
          countOccurrencesOf("Bank"),
          countOccurrencesOf("Haltestelle"),
          countOccurrencesOf("Leuchte"),
        ],
        backgroundColor: [
          "rgba(255, 99, 132, 0.2)",
          "rgba(54, 162, 235, 0.2)",
          "rgba(255, 206, 86, 0.2)",
        ],
        borderColor: [
          "rgba(255, 99, 132, 1)",
          "rgba(54, 162, 235, 1)",
          "rgba(255, 206, 86, 1)",
        ],
        borderWidth: 1,
      },
    ],
  };

  useEffect(() => {
    firebase
      .database()
      .ref("/serviceobjekte")
      .on("value", (snapshot) => {
        if (snapshot.val()) {
          let objects = [];
          const objectsValue = snapshot.val();
          const keys = Object.keys(objectsValue);
          for (var i = 0; i < keys.length; i++) {
            var k = keys[i];
            var Adresse = objectsValue[k].Adresse;
            var Active = objectsValue[k].Active;
            var CreateDate = objectsValue[k].CreateDate;
            var CreateUser = objectsValue[k].CreateUser;
            var Kaufdatum = objectsValue[k].Kaufdatum;
            var Latitude = objectsValue[k].Latitude;
            var Longitude = objectsValue[k].Longitude;
            var ObjectID = objectsValue[k].ObjectID;
            var Produkt = objectsValue[k].Produkt;
            var Produktbild = objectsValue[k].Produktbild;
            var RecID = objectsValue[k].RecID;
            var Zustand = objectsValue[k].Zustand;
            objects.push({
              Adresse,
              Active,
              CreateDate,
              CreateUser,
              Kaufdatum,
              Latitude,
              Longitude,
              ObjectID,
              Produkt,
              Produktbild,
              RecID,
              Zustand,
            });
          }
          setObjects(objects);
        }
      });
  }, []);

  return (
    <>
      <Grid
        container
        direction={"row"}
        style={{ height: "auto", paddingRight: 40 }}
      >
        <Grid item xs={7} style={{ height: "auto" }}>
          <div className={classes.root}>
            <Paper elevation={1}>
              <h1 style={{ marginTop: 0 }}>Standorte</h1>
              <ServiceObjekteMap objects={objects} />
            </Paper>
          </div>
        </Grid>
        <Grid item xs={5} style={{ height: "50%" }}>
          <div className={classes.root}>
            <Paper elevation={1}>
              <h1 style={{ marginTop: 0 }}>Kategorie</h1>
              <Doughnut data={data} />
            </Paper>
          </div>
        </Grid>
      </Grid>
      <Grid
        container
        direction={"row"}
        style={{ paddingRight: 40, paddingBottom: 40 }}
      >
        <Grid item xs={12}>
          <div className={classes.root}>
            <MaterialTable
              title={""}
              icons={tableIcons}
              options={{
                filtering: true,
                columnsButton: true,
                headerStyle: {
                  fontFamily: "'Open Sans', sans-serif",
                  backgroundColor: "#ddd",
                  fontWeight: 600,
                  textAlign: "left",
                },
              }}
              columns={[
                { title: "RecID", field: "RecID" },
                { title: "Objekt", field: "Produkt" },
                { title: "LAT", field: "Latitude" },
                { title: "LONG", field: "Longitude" },
                { title: "Adresse", field: "Adresse" },
                {
                  title: "Bewertung",
                  field: "Rating",
                  render: (rowData) => {
                    if (rowData.Zustand) {
                      let rating = 0;
                      let ratings = [];
                      const ratingsValue = rowData.Zustand;
                      const keys = Object.keys(ratingsValue);
                      for (var i = 0; i < keys.length; i++) {
                        var k = keys[i];
                        var Rating = ratingsValue[k].Rating;
                        ratings.push(Rating);
                      }
                      if (ratings.length == 0) {
                        rating = 0;
                      } else {
                        rating = (
                          ratings.reduce((acc, v) => acc + v) / ratings.length
                        ).toFixed(1);
                      }
                      console.log(rating);
                      return rating;
                    }
                    return null;
                  },
                },
              ]}
              data={objects}
              editable={[]}
              actions={[
                {
                  icon: () => <AddBox />,
                  tooltip: "Objekt hinzufügen",
                  isFreeAction: true,
                  onClick: () => setOpen(true),
                },
              ]}
            />
          </div>
        </Grid>
      </Grid>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle
          id="alert-dialog-title"
          style={{ paddingTop: 0, paddingBottom: 0 }}
        >
          <h1>Objekt erfassen</h1>
        </DialogTitle>
        <DialogContent>
          <AddObjectTable
            handleClose={handleClose}
            isFirstObject={
              objects.length === 0 ? 1 : objects.slice(-1).pop().RecID + 1
            }
          />
        </DialogContent>
      </Dialog>
    </>
  );
};

export default AdminServiceObjekte;
