import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import { Bar } from "react-chartjs-2";
import firebase from "firebase";
import ReactMapGL, { Marker } from "react-map-gl";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import ServiceObjekteMap from "../../components/serviceObjekteMap";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    height: "100%",
    "& > *": {
      marginLeft: theme.spacing(5),
      marginTop: theme.spacing(5),
      padding: theme.spacing(4),
      width: "100%",
    },
  },
}));

const AdminOverview = () => {
  const [object, setObject] = useState([]);
  const [incidents, setIncidents] = useState([]);
  const [wartungen, setWartungen] = useState([]);
  const [incidentsCounter, setIncidentsCounter] = useState([]);
  const classes = useStyles();

  const countOccurrencesOf = (search) => {
    if (incidents !== undefined) {
      return incidents.filter((el) => el.Status.includes(search)).length;
    }
  };

  const countOccurrencesOfserviceDate = (search) => {
    if (incidents !== undefined) {
      return wartungen.filter((el) => el.serviceDate.includes(search)).length;
    }
  };

  const dataIncidents = {
    labels: ["Abgeschlossen", "In Bearbeitung", "Offen"],
    datasets: [
      {
        label: "Anzahl pro Status",
        data: [
          countOccurrencesOf("Abgeschlossen"),
          countOccurrencesOf("In Bearbeitung"),
          countOccurrencesOf("Offen"),
        ],
        backgroundColor: [
          "rgba(255, 99, 132, 0.2)",
          "rgba(54, 162, 235, 0.2)",
          "rgba(255, 206, 86, 0.2)",
        ],
        borderColor: [
          "rgba(255, 99, 132, 1)",
          "rgba(54, 162, 235, 1)",
          "rgba(255, 206, 86, 1)",
        ],
        borderWidth: 1,
      },
    ],
  };

  const dataServices = {
    labels: [
      "14.07.2021",
      "15.07.2021",
      "16.07.2021",
      "17.07.2021",
      "18.07.2021",
      "19.07.2021",
    ],
    datasets: [
      {
        label: "Anzahl Services pro Tag",
        data: [2, 4, 0, 7, 3, 1],

        backgroundColor: [
          "rgba(255, 99, 132, 0.2)",
          "rgba(54, 162, 235, 0.2)",
          "rgba(255, 206, 86, 0.2)",
        ],
        borderColor: [
          "rgba(255, 99, 132, 1)",
          "rgba(54, 162, 235, 1)",
          "rgba(255, 206, 86, 1)",
        ],
        borderWidth: 1,
      },
    ],
  };

  useEffect(() => {
    firebase
      .database()
      .ref("/Incidents")
      .on("value", (snapshot) => {
        if (snapshot.val()) {
          let incidents = [];
          const incidentValue = snapshot.val();
          const keys = Object.keys(incidentValue);
          for (var i = 0; i < keys.length; i++) {
            var k = keys[i];
            var CreateDate = incidentValue[k].CreateDate;
            var Meldung = incidentValue[k].Meldung;
            var ObjectID = incidentValue[k].ObjectID;
            var Status = incidentValue[k].Status;
            incidents.push({ CreateDate, Meldung, ObjectID, ObjectID, Status });
          }

          setIncidents(incidents);
        }
      });
    firebase
      .database()
      .ref("/wartungen")
      .on("value", (snapshot) => {
        if (snapshot.val()) {
          let services = [];
          const wartungenValue = snapshot.val();
          const keys = Object.keys(wartungenValue);
          for (var i = 0; i < keys.length; i++) {
            var k = keys[i];
            var serviceDate = wartungenValue[k].serviceDatum;
            services.push({ serviceDate });
          }

          setWartungen(services);
        }
      });
    firebase
      .database()
      .ref("/serviceobjekte")
      .on("value", (snapshot) => {
        if (snapshot.val()) {
          let objects = [];
          const objectsValue = snapshot.val();
          const keys = Object.keys(objectsValue);
          for (var i = 0; i < keys.length; i++) {
            var k = keys[i];
            var Adresse = objectsValue[k].Adresse;
            var Active = objectsValue[k].Active;
            var CreateDate = objectsValue[k].CreateDate;
            var CreateUser = objectsValue[k].CreateUser;
            var Kaufdatum = objectsValue[k].Kaufdatum;
            var Latitude = objectsValue[k].Latitude;
            var Longitude = objectsValue[k].Longitude;
            var ObjectID = objectsValue[k].ObjectID;
            var Produkt = objectsValue[k].Produkt;
            var Produktbild = objectsValue[k].Produktbild;
            var RecID = objectsValue[k].RecID;
            var Zustand = objectsValue[k].Zustand;
            objects.push({
              Adresse,
              Active,
              CreateDate,
              CreateUser,
              Kaufdatum,
              Latitude,
              Longitude,
              ObjectID,
              Produkt,
              Produktbild,
              RecID,
              Zustand,
            });
          }
          setObject(objects);
        }
      });
  }, []);

  return (
    <>
      <Grid
        container
        direction={"row"}
        style={{ height: "auto", paddingRight: 40 }}
      >
        <Grid item xs={7}>
          <div className={classes.root}>
            <Paper elevation={1}>
              <h1 style={{ marginTop: 0 }}>Standorte</h1>
              <ServiceObjekteMap objects={object} />
            </Paper>
          </div>
        </Grid>
        <Grid item xs={5}>
          <div className={classes.root}>
            <Paper elevation={1}>
              <h1 style={{ marginTop: 0 }}>Incidents</h1>
              <div>
                <Bar
                  data={dataIncidents}
                  options={{
                    ticks: {
                      stepSize: 1,
                    },
                  }}
                />
              </div>
            </Paper>
          </div>
        </Grid>
      </Grid>
      <Grid
        container
        direction={"row"}
        style={{ paddingRight: 40, paddingBottom: 40 }}
      >
        <Grid item xs={12}>
          <div className={classes.root}>
            <Paper elevation={1}>
              <h1 style={{ marginTop: 0 }}>
                Wartungs-Aufträge pro Tag / Objekt
              </h1>
              <div>
                <Bar data={dataServices} />
              </div>
            </Paper>
          </div>
        </Grid>
      </Grid>
    </>
  );
};

export default AdminOverview;
