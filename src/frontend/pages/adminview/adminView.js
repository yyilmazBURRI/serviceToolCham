import React from "react";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import SideNav from "../../components/sidenav";

import AdminOverview from "./adminOverview";
import AdminServiceObjekte from "./adminServiceObjekte";

import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import AdminWartung from "./adminWartung";
import AdminIncidents from "./adminIncidents";
import AdminReports from "./adminReports";
import AdminSupport from "./adminSupport";
import Logo from "../../../assets/logo.png";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

const AdminView = () => {
  const classes = useStyles();
  return (
    <>
      <AppBar position="sticky" style={{ backgroundColor: "rgb(64, 64, 64)" }}>
        <Toolbar>
          <img src={Logo} alt="logo" style={{ height: 45 }} />
        </Toolbar>
      </AppBar>
      <Grid container direction={"row"}>
        <Grid
          item
          sm={2}
          xl={1}
          style={{
            backgroundColor: "white",
            borderRight: "1px solid #BFBFBF",
          }}
        >
          <SideNav />
        </Grid>
        <Grid item sm={10} xl={11}>
          <Switch>
            <Route path="/admin/overview" component={AdminOverview} />
            <Route
              exact
              path="/admin/serviceobjekte"
              component={AdminServiceObjekte}
            />
            <Route path="/admin/wartung" component={AdminWartung} />
            <Route exact path="/admin/incidents" component={AdminIncidents} />
            <Route exact path="/admin/reports" component={AdminReports} />
            <Route exact path="/admin/support" component={AdminSupport} />
          </Switch>
        </Grid>
      </Grid>
    </>
  );
};

export default AdminView;
