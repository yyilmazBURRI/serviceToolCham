import React, { useEffect, useState, forwardRef } from "react";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import firebase from "firebase";
import { Grid } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import MaterialTable from "material-table";

import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

import AddBox from "@material-ui/icons/AddBox";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",

    height: "100%",
    "& > *": {
      marginLeft: theme.spacing(5),
      marginTop: theme.spacing(5),
      padding: theme.spacing(4),
      width: "100%",
    },
  },
  button: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    width: "100%",
    border: "1px solid #000000",
    borderRadius: 0,
    padding: 16,
    paddingTop: 8,
    paddingBottom: 8,
    fontWeight: "bold",
    fontSize: 12,
    textTransform: "uppercase",
    backgroundColor: "black",
    color: "white",
    fill: "white",
  },
}));

const AdminReports = () => {
  const classes = useStyles();
  const [wartungen, setWartungen] = useState([]);
  const [image, setImage] = useState("");
  const [incidents, setIncidents] = useState([]);
  const [open, setIsOpen] = useState(false);

  const handleClickOpen = (image) => {
    setImage(image);
    setIsOpen(true);
  };

  const handleClose = () => {
    setIsOpen(false);
  };

  useEffect(() => {
    firebase
      .database()
      .ref("/wartungen")
      .on("value", (snapshot) => {
        if (snapshot.val()) {
          let wartungen = [];
          const wartungenValue = snapshot.val();
          const keys = Object.keys(wartungenValue);
          for (var i = 0; i < keys.length; i++) {
            var k = keys[i];
            var RecID = wartungenValue[k].RecID;
            var ObjektID = wartungenValue[k].ObjectID;
            var Objekt = wartungenValue[k].Objekt;
            var Adresse = wartungenValue[k].Adresse;
            var Zustand = wartungenValue[k].Zustand;
            var Task = wartungenValue[k].Task;
            var Bemerkungen = wartungenValue[k].Bemerkungen;
            var Status = wartungenValue[k].Status;
            var serviceDatum = wartungenValue[k].serviceDatum;
            var Servicebild = wartungenValue[k].Servicebild;
            var MA = wartungenValue[k].MA;
            var DatumErledigt = wartungenValue[k].DatumErledigt;
            if (Status === "Abgeschlossen") {
              wartungen.push({
                MA,
                RecID,
                ObjektID,
                Objekt,
                Adresse,
                Task,
                Bemerkungen,
                Zustand,
                Status,
                serviceDatum,
                Servicebild,
                DatumErledigt,
              });
            }
          }
          setWartungen(wartungen);
        }
      });
    firebase
      .database()
      .ref("/Incidents")
      .on("value", (snapshot) => {
        if (snapshot.val()) {
          let incidents = [];
          const incidentsvalue = snapshot.val();
          const keys = Object.keys(incidentsvalue);
          for (var i = 0; i < keys.length; i++) {
            var k = keys[i];
            var CreateDate = incidentsvalue[k].CreateDate;
            var Meldung = incidentsvalue[k].Meldung;
            var ObjectID = incidentsvalue[k].ObjectID;
            var Status = incidentsvalue[k].Status;
            var Produktbild = incidentsvalue[k].Produktbild;
            var MA = incidentsvalue[k].MA;
            var DatumErledigt = incidentsvalue[k].DatumErledigt;

            if (Status === "Abgeschlossen") {
              incidents.push({
                MA,
                Produktbild,
                CreateDate,
                Meldung,
                ObjectID,
                Status,
                DatumErledigt,
              });
            }
          }
          setIncidents(incidents);
        }
      });
  }, []);

  return (
    <>
      <Grid container direction={"row"} style={{ paddingRight: 40 }}>
        <Grid item xs={12}>
          <div className={classes.root}>
            <Paper elevation={1}>
              <h1 style={{ marginTop: 0 }}>Alle quittierten Incidents</h1>
              <MaterialTable
                title={""}
                icons={tableIcons}
                options={{
                  filtering: true,
                  columnsButton: true,
                  headerStyle: {
                    fontFamily: "'Open Sans', sans-serif",
                    backgroundColor: "#ddd",
                    fontWeight: 600,
                    textAlign: "left",
                  },
                }}
                columns={[
                  { title: "Objekt", field: "ObjectID" },
                  { title: "Erledigt am", field: "DatumErledigt" },
                  { title: "Mitarbeiter", field: "MA" },
                  { title: "Meldung", field: "Meldung" },
                  {
                    title: "Bild",
                    field: "Produktbild",
                    render: (rowData) =>
                      rowData.Produktbild !== undefined ? (
                        <button
                          className={classes.button}
                          onClick={() => handleClickOpen(rowData.Produktbild)}
                        >
                          Bild anzeigen
                        </button>
                      ) : (
                        "Kein Bild"
                      ),
                  },
                ]}
                components={{
                  Container: (props) => <Paper {...props} elevation={0} />,
                }}
                data={incidents}
              />
            </Paper>
          </div>
        </Grid>
      </Grid>
      <Grid
        container
        direction={"row"}
        style={{ paddingRight: 40, paddingBottom: 40 }}
      >
        <Grid item xs={12}>
          <div className={classes.root}>
            <Paper elevation={1}>
              <h1 style={{ marginTop: 0 }}>
                Alle quittierten Wartungs-Aufträge
              </h1>
              <MaterialTable
                title={""}
                icons={tableIcons}
                options={{
                  filtering: true,
                  columnsButton: true,
                  headerStyle: {
                    fontFamily: "'Open Sans', sans-serif",
                    backgroundColor: "#ddd",
                    fontWeight: 600,
                    textAlign: "left",
                  },
                }}
                columns={[
                  { title: "Objekt", field: "ObjektID" },
                  { title: "Erledigt am", field: "DatumErledigt" },
                  { title: "Mitarbeiter", field: "MA" },
                  { title: "Task", field: "Task" },
                  { title: "Bemerkungen", field: "Bemerkungen" },
                  {
                    title: "Bild",
                    field: "Servicebild",
                    render: (rowData) =>
                      rowData.Servicebild !== undefined ? (
                        <button
                          className={classes.button}
                          onClick={() => handleClickOpen(rowData.Servicebild)}
                        >
                          Bild anzeigen
                        </button>
                      ) : (
                        "Kein Bild"
                      ),
                  },
                ]}
                components={{
                  Container: (props) => <Paper {...props} elevation={0} />,
                }}
                data={wartungen}
              />
            </Paper>
          </div>
        </Grid>
      </Grid>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <img src={image} style={{ width: "100%" }} alt="Bild von Arbeit" />
        </DialogContent>
      </Dialog>
    </>
  );
};

export default AdminReports;
