import React, { useEffect, useState, forwardRef } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import { Bar } from "react-chartjs-2";
import firebase from "firebase";
import MaterialTable from "material-table";
import { Doughnut } from "react-chartjs-2";

import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

import AddObjectTable from "../../components/addObject";

import AddBox from "@material-ui/icons/AddBox";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";
import IncidentsMap from "../../components/incidentsMap";

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    height: "100%",
    "& > *": {
      marginLeft: theme.spacing(5),
      marginTop: theme.spacing(5),
      padding: theme.spacing(4),
      width: "100%",
    },
  },
}));

export default () => {
  const [objects, setObjects] = useState([]);
  const [open, setOpen] = useState(false);

  const [viewport, setViewport] = useState({
    width: "100%",
    height: "100%",
    latitude: 47.17829041524882,
    longitude: 8.46158514871517,
    zoom: 12,
  });

  const [incidents, setIncidents] = useState([]);
  const [activeIncidents, setActiveIncidents] = useState([]);
  const [labelArray, setLabelArray] = useState([]);
  const [countIncidents, setCountIncidents] = useState([]);

  const classes = useStyles();

  const handleClose = () => {
    setOpen(false);
  };

  const filterArray = () => {
    if (incidents.length > 0) {
      const arrayHolder = incidents.filter(
        (v, i, a) => a.findIndex((t) => t.ObjectID === v.ObjectID) === i
      );

      const holderFilter = arrayHolder.map((incident) => incident.ObjectID);
      return holderFilter;
    }
  };

  const countIncident = () => {
    if (incidents.length > 0) {
      const map = incidents.reduce(
        (incident, e) =>
          incident.set(e.ObjectID, (incident.get(e.ObjectID) || 0) + 1),
        new Map()
      );
      return [...map.values()];
    }
  };

  const data = {
    labels: filterArray(),

    datasets: [
      {
        label: "Anzahl der eingangenen Meldung pro Objekt",
        data: countIncident(),
        backgroundColor: [
          "rgba(255, 99, 132, 0.2)",
          "rgba(54, 162, 235, 0.2)",
          "rgba(255, 206, 86, 0.2)",
        ],
        borderColor: [
          "rgba(255, 99, 132, 1)",
          "rgba(54, 162, 235, 1)",
          "rgba(255, 206, 86, 1)",
        ],
        borderWidth: 1,
      },
    ],
  };

  const options = {
    indexAxis: "y",
    // Elements options apply to all of the options unless overridden in a dataset
    // In this case, we are setting the border of each horizontal bar to be 2px wide
    elements: {
      bar: {
        borderWidth: 2,
      },
    },
    responsive: true,
    plugins: {
      legend: {
        position: "bottom",
      },
    },
    ticks: {
      stepSize: 1,
    },
  };

  useEffect(() => {
    firebase
      .database()
      .ref("/serviceobjekte")
      .on("value", (snapshot) => {
        if (snapshot.val()) {
          let objects = [];
          let incidents = [];
          const objectsValue = snapshot.val();
          const keys = Object.keys(objectsValue);
          for (var i = 0; i < keys.length; i++) {
            var k = keys[i];
            var Adresse = objectsValue[k].Adresse;
            var Active = objectsValue[k].Active;
            var CreateDate = objectsValue[k].CreateDate;
            var CreateUser = objectsValue[k].CreateUser;
            var Kaufdatum = objectsValue[k].Kaufdatum;
            var Latitude = objectsValue[k].Latitude;
            var Longitude = objectsValue[k].Longitude;
            var ObjectID = objectsValue[k].ObjectID;
            var Produkt = objectsValue[k].Produkt;
            var Produktbild = objectsValue[k].Produktbild;
            var RecID = objectsValue[k].RecID;
            var Zustand = objectsValue[k].Zustand;
            var Incidents = objectsValue[k].incidents;
            objects.push({
              Adresse,
              Active,
              CreateDate,
              CreateUser,
              Kaufdatum,
              Latitude,
              Longitude,
              ObjectID,
              Produkt,
              Produktbild,
              RecID,
              Zustand,
              Incidents,
            });
          }
          setObjects(objects);
        }
      });
    firebase
      .database()
      .ref("/Incidents")
      .on("value", (snapshot) => {
        if (snapshot.val()) {
          let incidents = [];
          const incidentsvalue = snapshot.val();
          const keys = Object.keys(incidentsvalue);
          for (var i = 0; i < keys.length; i++) {
            var k = keys[i];
            var CreateDate = incidentsvalue[k].CreateDate;
            var Meldung = incidentsvalue[k].Meldung;
            var ObjectID = incidentsvalue[k].ObjectID;
            var Status = incidentsvalue[k].Status;
            var Longitude = incidentsvalue[k].Longitude;
            var Latitude = incidentsvalue[k].Latitude;
            incidents.push({
              CreateDate,
              Meldung,
              ObjectID,
              Status,
              Longitude,
              Latitude,
            });
          }
          setActiveIncidents(
            incidents.filter((incident) => incident.Status === "Offen")
          );
          setIncidents(incidents);
        }
      });
  }, []);

  return (
    <>
      <Grid container direction={"row"} style={{ paddingRight: 40 }}>
        <Grid item xs={7}>
          <div className={classes.root}>
            <Paper elevation={1}>
              <h1 style={{ marginTop: 0 }}>Standorte</h1>
              <IncidentsMap incidents={incidents} />
            </Paper>
          </div>
        </Grid>
        <Grid item xs={5}>
          <div className={classes.root}>
            <Paper elevation={1}>
              <h1 style={{ marginTop: 0 }}>Kategorie</h1>
              <Bar data={data} options={options} />
            </Paper>
          </div>
        </Grid>
      </Grid>
      <Grid
        container
        direction={"row"}
        style={{ paddingRight: 40, paddingBottom: 40 }}
      >
        <Grid item xs={12}>
          <div className={classes.root}>
            <MaterialTable
              title={""}
              icons={tableIcons}
              options={{
                filtering: true,
                columnsButton: true,
                headerStyle: {
                  fontFamily: "'Open Sans', sans-serif",
                  backgroundColor: "#ddd",
                  fontWeight: 600,
                  textAlign: "left",
                },
              }}
              columns={[
                { title: "Objekt", field: "ObjectID" },
                { title: "Eingangsdatum", field: "CreateDate" },
                { title: "Meldung", field: "Meldung" },
                { title: "Status", field: "Status" },
              ]}
              data={incidents}
            />
          </div>
        </Grid>
      </Grid>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle
          id="alert-dialog-title"
          style={{ paddingTop: 0, paddingBottom: 0 }}
        >
          <h1>Objekt erfassen</h1>
        </DialogTitle>
        <DialogContent>
          <AddObjectTable
            handleClose={handleClose}
            isFirstObject={objects.length === 0 ? 1 : objects.length + 1}
          />
        </DialogContent>
      </Dialog>
    </>
  );
};
