import React, { useState } from "react";

import { makeStyles } from "@material-ui/core/styles";
import { Grid } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import File1 from "../../../assets/files/lorem-ipsum.pdf";

import InputLabel from "@material-ui/core/InputLabel";
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import emailjs, { init } from "emailjs-com";
import "react-notifications/lib/notifications.css";
import {
  NotificationContainer,
  NotificationManager,
} from "react-notifications";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    height: "100%",
    "& > *": {
      marginLeft: theme.spacing(5),
      marginTop: theme.spacing(5),
      padding: theme.spacing(4),
      width: "100%",
    },
  },
  uilabel: {
    textTransform: "uppercase",
    color: "black",
    fontSize: 11,
    fontWeight: "bold",
    paddingBottom: 4,
    paddingTop: 16,
  },
  uiinput: {
    backgroundColor: "#EDEDED",
    border: "1px solid #D9D9D9",
    borderRadius: 0,
    padding: 4,
    paddingLeft: 8,
    paddingRight: 8,
    cursor: "text",
    fontWeight: "bold",
    fontSize: 16,
    width: 230,
    maxHeight: 28,
    margin: 5,
    textDecoration: "none",
    transitionProperty: "color, border-color",
    transitionDuration: 0.1,
    transitionTimingFunction: "ease-in-out",
    boxShadow: "none",
  },

  buttons2: {
    marginTop: 15,
    width: 100,
    border: "1px solid black",
    padding: 16,
    borderRadius: 0,
    paddingTop: 8,
    paddingBottom: 8,
    fontWeight: "bold",
    fontSize: 12,
    textTransform: "uppercase",
    backgroundColor: "black",
    color: "white",
    fill: "white",
    "-webkit-user-select": "none",
    "-ms-user-select": "none",
    "user-select": "none",

    "&:hover": {
      backgroundColor: "#000000",
      color: "white",
      fill: "white",
      borderColor: "#000000",
    },
  },
  selectBox: {
    backgroundColor: "#EDEDED",
    borderRadius: 2.5,
    padding: 15,
    fontFamily: "'Open Sans', sans-serif",
    transition: "all 0.1s ease-in-out",
    "&:hover": {
      borderLeft: "10px black solid",
    },
  },
}));

const AdminSupport = () => {
  init("user_MOmmaATw464v5phEJLU2u");
  const classes = useStyles();
  const [Name, setName] = useState("");
  const [Kategorie, setKategorie] = useState("");
  const [Mitteilung, setMitteilung] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(e);

    emailjs
      .sendForm(
        `service_02rlhde`,
        "template_4vl0ruk",
        e.target,
        "user_MOmmaATw464v5phEJLU2u"
      )
      .then(
        (result) => {
          NotificationManager.success(
            "Das Email wurde erfolgreich versendet",
            "Erfolg!"
          );
          setName("");
          setKategorie("");
          setMitteilung("");
        },
        (error) => {
          alert("An error occurred, Please try again", error.text);
        }
      );
  };
  return (
    <form onSubmit={handleSubmit}>
      <Grid
        container
        direction={"row"}
        style={{ height: "50%", paddingRight: 40 }}
      >
        <Grid item xs={7}>
          <div className={classes.root}>
            <Paper elevation={1}>
              <h1 style={{ marginTop: 0 }}>Support</h1>
              <Grid container direction="row">
                <Grid item xs={12}>
                  <InputLabel
                    className={classes.uilabel}
                    htmlFor="Projektnummer-simple"
                  >
                    Name*
                  </InputLabel>
                  <input
                    name={"from_name"}
                    className={classes.uiinput}
                    type="text"
                    value={Name}
                    onChange={(e) => setName(e.target.value)}
                  />
                </Grid>
                <Grid item xs={12}>
                  <InputLabel
                    htmlFor="artikelcode-simple"
                    className={classes.uilabel}
                  >
                    Kategorie*
                  </InputLabel>
                  <Select
                    disableUnderline
                    className={classes.uiinput}
                    defaultValue={""}
                    value={Kategorie}
                    onChange={(e) => setKategorie(e.target.value)}
                    inputProps={{
                      name: "from_kategorie",
                      id: "kategorie-simple",
                    }}
                  >
                    <MenuItem value="">
                      <em>-</em>
                    </MenuItem>
                    <MenuItem value="Technische Hilfe">
                      <p>Technische Hilfe</p>
                    </MenuItem>
                    <MenuItem value="Bedienungsfrage">
                      <p>Bedienungsfrage</p>
                    </MenuItem>
                    <MenuItem value="Allgemein">
                      <p>Allgemein</p>
                    </MenuItem>
                  </Select>
                </Grid>
                <Grid item xs={12}>
                  <InputLabel
                    className={classes.uilabel}
                    htmlFor="Projektnummer-simple"
                  >
                    Anliegen*
                  </InputLabel>
                  <textarea
                    name={"message"}
                    type="text"
                    value={Mitteilung}
                    className={classes.uiinput}
                    onChange={(e) => setMitteilung(e.target.value)}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Button
                    className={classes.buttons2}
                    disabled={Name !== "" && Mitteilung !== "" ? false : true}
                    type="submit"
                    variant="contained"
                    color="primary"
                  >
                    Senden
                  </Button>
                </Grid>
              </Grid>
            </Paper>
          </div>
        </Grid>
        <Grid item xs={5}>
          <div className={classes.root}>
            <Paper elevation={1}>
              <h1 style={{ marginTop: 0 }}>Dokumente</h1>
              <div
                className={classes.selectBox}
                onClick={(e) => window.open(File1)}
              >
                Test.pdf
              </div>
            </Paper>
          </div>
        </Grid>
      </Grid>
      <NotificationContainer />
    </form>
  );
};

export default AdminSupport;
