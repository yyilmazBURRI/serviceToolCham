import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Select from "@material-ui/core/Select";
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import DateFnsUtils from "@date-io/date-fns";
import firebase from "firebase";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import ImageUploading, { ImageListType } from "react-images-uploading";

import InputLabel from "@material-ui/core/InputLabel";
import { DropzoneArea } from "material-ui-dropzone";

import moment from "moment";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  datePickerField: {
    minWidth: 140,
  },
  gridItem: {
    padding: 5,
    overflow: "hidden",
  },
  inputContainer: {
    width: "100%",
  },

  uilabel: {
    textTransform: "uppercase",
    color: "black",
    fontSize: 11,
    fontWeight: "bold",
    paddingBottom: 4,
    // paddingTop: 16,
  },

  uiinput: {
    backgroundColor: "#EDEDED",
    border: "1px solid #D9D9D9",
    borderRadius: 0,
    padding: 4,
    paddingLeft: 8,
    paddingRight: 8,
    cursor: "text",
    fontWeight: "bold",
    fontSize: 16,
    width: 230,
    maxHeight: 28,
    margin: 5,
    textDecoration: "none",
    transitionProperty: "color, border-color",
    transitionDuration: 0.1,
    transitionTimingFunction: "ease-in-out",
    boxShadow: "none",
  },

  buttons1: {
    width: 100,
    border: "1px solid #000000",
    borderRadius: 0,
    padding: 16,
    paddingTop: 8,
    paddingBottom: 8,
    fontWeight: "bold",
    fontSize: 12,
    textTransform: "uppercase",
    backgroundColor: "white",
    color: "#404040",
    fill: "#404040",
    "-webkit-user-select": "none",
    "-ms-user-select": "none",
    "user-select": "none",

    "&:hover": {
      backgroundColor: "#000000",
      color: "white",
      fill: "white",
      borderColor: "#000000",
    },
  },

  buttons2: {
    width: 100,
    border: "1px solid black",
    padding: 16,
    borderRadius: 0,

    paddingTop: 8,
    paddingBottom: 8,
    fontWeight: "bold",
    fontSize: 12,
    textTransform: "uppercase",
    backgroundColor: "black",
    color: "white",
    fill: "white",
    "-webkit-user-select": "none",
    "-ms-user-select": "none",
    "user-select": "none",

    "&:hover": {
      backgroundColor: "#000000",
      color: "white",
      fill: "white",
      borderColor: "#000000",
    },
  },
}));

const AddWartung = (props) => {
  const objects = props.objects;
  const [Objekt, setObjekt] = useState({});
  const [Bemerkungen, setBemerkungen] = useState("");
  const [RecID, setRecID] = useState(props.isFirstObject);
  const [Zustand, setZustand] = useState("");
  const [serviceDatum, setServiceDatum] = useState(null);
  const [Task, setTask] = useState("");

  const handleClose = props.handleClose;

  let newWartung = {
    serviceDatum: moment(serviceDatum).format("DD-MM-YYYY"),
    RecID: RecID,
    Adresse: Objekt.Adresse,
    ObjectID: Objekt.RecID,
    ObjectUID: Objekt.ObjectID,
    Objekt: Objekt.Produkt,
    Bemerkungen: Bemerkungen,
    Zustand: Zustand,
    Task: Task,
    Status: "Offen",
    Longitude: Objekt.Longitude,
    Latitude: Objekt.Latitude,
  };

  const handleSubmit = () => {
    const push = firebase
      .database()
      .ref("/wartungen/")
      .push(newWartung)
      .then((push) => {
        firebase
          .database()
          .ref(
            "serviceobjekte/" + Objekt.ObjectID + "/service/" + push.getKey()
          )
          .update({
            Bemerkungen: Bemerkungen,
            Task: Task,
            Status: "Offen",
            serviceDatum: moment(serviceDatum).format("DD-MM-YYYY"),
            wartungID: push.getKey(),
          });
        handleClose();
      });
  };

  const handleChange = (e) => {
    setObjekt({
      RecID: e.RecID,
      Produkt: e.Produkt,
      Adresse: e.Adresse,
      ObjectID: e.ObjectID,
      Longitude: e.Longitude,
      Latitude: e.Latitude,
    });
  };

  const classes = useStyles();
  return (
    <div>
      <Grid container direction="row" justify="center" alignItems="center">
        <Grid item xs={6} className={classes.gridItem}>
          <InputLabel htmlFor="artikelcode-simple" className={classes.uilabel}>
            Objekt*
          </InputLabel>
          <Select
            disableUnderline
            className={classes.uiinput}
            defaultValue={""}
            value={Objekt.Produkt}
            onChange={(e) => handleChange(e.target.value)}
            label={"Artikelcode*"}
            inputProps={{
              name: "Artikelcode",
              id: "artikelcode-simple",
            }}
            renderValue={(value) => (
              <div>
                {value.RecID} | {value.Produkt}
              </div>
            )}
          >
            <MenuItem value="">
              <em>-</em>
            </MenuItem>
            {objects.map((object, index) => {
              return (
                <MenuItem
                  value={{
                    Produkt: object.Produkt,
                    RecID: object.RecID,
                    Adresse: object.Adresse,
                    ObjectID: object.ObjectID,
                    Longitude: object.Longitude,
                    Latitude: object.Latitude,
                  }}
                >
                  {object.RecID} | {object.Produkt}
                </MenuItem>
              );
            })}
          </Select>
        </Grid>
        <Grid item xs={6} className={classes.gridItem}>
          <InputLabel htmlFor="Lieferdatum-simple" className={classes.uilabel}>
            Servicedatum*
          </InputLabel>
          <KeyboardDatePicker
            clearable
            InputProps={{
              disableUnderline: true,
            }}
            className={classes.uiinput}
            value={serviceDatum}
            onChange={(date) => setServiceDatum(moment(date).format())}
            format="DD-MM-YYYY"
          />
        </Grid>
        <Grid item xs={6} className={classes.gridItem}>
          <InputLabel
            htmlFor="Artikelnummer-simple"
            className={classes.uilabel}
          >
            Zustand*
          </InputLabel>
          <Select
            disableUnderline
            className={classes.uiinput}
            defaultValue={""}
            value={Zustand}
            onChange={(e) => setZustand(e.target.value)}
            inputProps={{
              name: "Zustand",
              id: "Zustand-simple",
            }}
          >
            <MenuItem value="">
              <em>-</em>
            </MenuItem>
            <MenuItem value="Schlecht">
              <p>Schlecht</p>
            </MenuItem>
            <MenuItem value="Mittel">
              <p>Mittel</p>
            </MenuItem>
            <MenuItem value="Gut">
              <p>Gut</p>
            </MenuItem>
          </Select>
        </Grid>
        <Grid item xs={6} className={classes.gridItem}>
          <InputLabel htmlFor="Beschreibung-simple" className={classes.uilabel}>
            Bemerkungen*
          </InputLabel>
          <textarea
            className={classes.uiinput}
            required
            id="Beschreibung-required"
            value={Bemerkungen}
            onChange={(e) => setBemerkungen(e.target.value)}
          />
        </Grid>
        <Grid item xs={6} className={classes.gridItem}>
          <InputLabel htmlFor="Task-simple" className={classes.uilabel}>
            Task*
          </InputLabel>
          <Select
            disableUnderline
            className={classes.uiinput}
            defaultValue={""}
            value={Task}
            onChange={(e) => setTask(e.target.value)}
            inputProps={{
              name: "Task",
              id: "Task-simple",
            }}
          >
            <MenuItem value="">
              <em>-</em>
            </MenuItem>
            <MenuItem value="Reinigung">
              <p>Reinigung</p>
            </MenuItem>
            <MenuItem value="Reparatur">
              <p>Reparatur</p>
            </MenuItem>
            <MenuItem value="Lattenersatz">
              <p>Lattenersatz</p>
            </MenuItem>
          </Select>
        </Grid>

        <Grid item xs={6} align="right" className={classes.gridItem}>
          <Button
            className={classes.buttons1}
            variant="contained"
            color="primary"
            // disabled={
            //   Objektkategorie !== "" &&
            //   Lat !== 0 &&
            //   Long !== 0 &&
            //   Adresse !== "" &&
            //     ? false
            //     : true
            // }
            onClick={() => handleSubmit()}
          >
            Anlegen
          </Button>
          <Button
            className={classes.buttons2}
            variant="contained"
            color="primary"
            autoFocus
            onClick={() => handleClose()}
          >
            Abbrechen
          </Button>
        </Grid>
      </Grid>
    </div>
  );
};

export default AddWartung;
