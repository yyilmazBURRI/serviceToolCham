import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useLocation,
} from "react-router-dom";

const useStyles = makeStyles(() => ({
  selectBox: {
    borderTop: "1px solid #BFBFBF",
    paddingLeft: 16,
    paddingTop: 8,
    paddingBottom: 8,
    height: 36,
    backgroundColor: "#EDEDED",
    borderRadius: 2.5,
    padding: 15,
    fontFamily: "'Open Sans', sans-serif",
    transition: "all 0.1s ease-in-out",
    "&:hover": {
      borderLeft: "4px solid rgb(0,120,191)",
    },
  },
  selectBoxActive: {
    borderTop: "1px solid #BFBFBF",
    paddingLeft: 16,
    paddingTop: 8,
    paddingBottom: 8,
    height: 36,
    backgroundColor: "#EDEDED",
    borderRadius: 2.5,
    padding: 15,
    fontFamily: "'Open Sans', sans-serif",
    transition: "all 0.1s ease-in-out",
    borderLeft: "4px solid rgb(0,120,191)",
  },

  link: {
    textDecoration: "none",
    color: "black",
  },
}));

const SideNav = (props) => {
  const classes = useStyles();
  return (
    <ul style={{ position: "sticky", width: "100%" }}>
      <Link key={1} to={"/admin/overview"} className={classes.link}>
        <li
          className={
            useLocation().pathname === "/admin/overview"
              ? classes.selectBoxActive
              : classes.selectBox
          }
          style={{ borderTop: "none" }}
        >
          Übersicht
        </li>
      </Link>
      <Link to={"/admin/serviceobjekte"} className={classes.link}>
        <li
          className={
            useLocation().pathname === "/admin/serviceobjekte"
              ? classes.selectBoxActive
              : classes.selectBox
          }
        >
          Serviceobjekte
        </li>
      </Link>
      <Link key={3} to={"/admin/wartung"} className={classes.link}>
        <li
          className={
            useLocation().pathname === "/admin/wartung"
              ? classes.selectBoxActive
              : classes.selectBox
          }
        >
          Wartung
        </li>
      </Link>
      <Link key={4} to={"/admin/incidents"} className={classes.link}>
        <li
          className={
            useLocation().pathname === "/admin/incidents"
              ? classes.selectBoxActive
              : classes.selectBox
          }
        >
          Incidents
        </li>
      </Link>
      <Link key={5} to={"/admin/reports"} className={classes.link}>
        <li
          className={
            useLocation().pathname === "/admin/reports"
              ? classes.selectBoxActive
              : classes.selectBox
          }
        >
          Reports
        </li>
      </Link>
      <Link key={6} to={"/admin/support"} className={classes.link}>
        <li
          className={
            useLocation().pathname === "/admin/support"
              ? classes.selectBoxActive
              : classes.selectBox
          }
          style={{
            backgroundColor: "white",
            borderBottom: "1px solid #BFBFBF",
          }}
        >
          Support & Hilfe
        </li>
      </Link>
    </ul>
  );
};

export default SideNav;
