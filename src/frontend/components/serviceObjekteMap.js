import React, { useState } from "react";

import ReactMapGL, { Marker } from "react-map-gl";
import LocationOnIcon from "@material-ui/icons/LocationOn";

const Map = (props) => {
  const [viewport, setViewport] = useState({
    width: "100%",
    height: "80%",
    latitude: 47.17829041524882,
    longitude: 8.46158514871517,
    zoom: 15,
  });
  return (
    <ReactMapGL
      mapboxApiAccessToken={
        "pk.eyJ1IjoiYnVycmkiLCJhIjoiY2tuc2loMWs4MmkydzJxbnhidmFkYmRtYyJ9.OM79ZdZ8JN6JY7j30TQ9aw"
      }
      {...viewport}
      onViewportChange={(nextViewport) => setViewport(nextViewport)}
      mapStyle={"mapbox://styles/mapbox/basic-v9"}
    >
      {props.objects.map((object) => {
        return (
          <Marker latitude={object.Latitude} longitude={object.Longitude}>
            <div>
              <LocationOnIcon />{" "}
              {object.Produkt + " " + object.RecID + " steht hier"}
              <img
                src={object.Produktbild}
                style={{ height: 50, width: 50, borderRadius: "50%" }}
                alt="Produktbild"
              />
            </div>
          </Marker>
        );
      })}
    </ReactMapGL>
  );
};

export default Map;
