import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Select from "@material-ui/core/Select";
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import firebase from "firebase";
import { KeyboardDatePicker } from "@material-ui/pickers";

import InputLabel from "@material-ui/core/InputLabel";
import { DropzoneArea } from "material-ui-dropzone";

import moment from "moment";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },

  input: {
    width: 0.1,
    height: 0.1,
    opacity: 0,
    overflow: "hidden",
    position: "absolute",
    zIndex: -1,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  datePickerField: {
    minWidth: 140,
  },
  gridItem: {
    padding: 5,
    overflow: "hidden",
  },
  inputContainer: {
    width: "100%",
  },

  uilabel: {
    textTransform: "uppercase",
    color: "black",
    fontSize: 11,
    fontWeight: "bold",
    paddingBottom: 4,
    // paddingTop: 16,
  },

  uiinput: {
    backgroundColor: "#EDEDED",
    border: "1px solid #D9D9D9",
    borderRadius: 0,
    padding: 4,
    paddingLeft: 8,
    paddingRight: 8,
    cursor: "text",
    fontWeight: "bold",
    fontSize: 16,
    width: 230,
    maxHeight: 28,
    margin: 5,
    textDecoration: "none",
    transitionProperty: "color, border-color",
    transitionDuration: 0.1,
    transitionTimingFunction: "ease-in-out",
    boxShadow: "none",
  },

  buttons1: {
    width: 100,
    border: "1px solid #000000",
    borderRadius: 0,
    padding: 16,
    paddingTop: 8,
    paddingBottom: 8,
    fontWeight: "bold",
    fontSize: 12,
    textTransform: "uppercase",
    backgroundColor: "white",
    color: "#404040",
    fill: "#404040",
    "-webkit-user-select": "none",
    "-ms-user-select": "none",
    "user-select": "none",

    "&:hover": {
      backgroundColor: "#000000",
      color: "white",
      fill: "white",
      borderColor: "#000000",
    },
  },

  buttons2: {
    width: 100,
    border: "1px solid black",
    padding: 16,
    borderRadius: 0,

    paddingTop: 8,
    paddingBottom: 8,
    fontWeight: "bold",
    fontSize: 12,
    textTransform: "uppercase",
    backgroundColor: "black",
    color: "white",
    fill: "white",
    "-webkit-user-select": "none",
    "-ms-user-select": "none",
    "user-select": "none",

    "&:hover": {
      backgroundColor: "#000000",
      color: "white",
      fill: "white",
      borderColor: "#000000",
    },
  },
}));

const AddObjectTable = (props) => {
  const [files, setFiles] = useState({});
  const [Objektkategorie, setObjektKategorie] = useState("");
  const [ObjektBild, setObjektBild] = useState("");
  const [RecID, setRecID] = useState(props.isFirstObject);
  const [Lat, setLat] = useState(0);
  const [Long, setLong] = useState(0);
  const [Adresse, setAdresse] = useState("");
  const [Kaufdatum, setKaufdatum] = useState(null);

  const handleClose = props.handleClose;

  let newOrder = {
    RecID: RecID,
    Produkt: Objektkategorie,
    Active: true,
    Longitude: Long,
    Latitude: Lat,
    CreateDate: moment().format("DD-MM-YYYY"),
    Kaufdatum: moment(Kaufdatum).format("DD-MM-YYYY"),
    CreateUser: "yasin",
    Produktbild: ObjektBild,
    Adresse: Adresse,
  };

  const handleSubmit = () => {
    const push = firebase
      .database()
      .ref("/serviceobjekte/")
      .push({
        RecID: newOrder.RecID,
        Produkt: newOrder.Produkt,
        Active: newOrder.Active,
        Longitude: parseFloat(newOrder.Longitude),
        Latitude: parseFloat(newOrder.Latitude),
        CreateDate: newOrder.CreateDate,
        Kaufdatum: newOrder.Kaufdatum,
        CreateUser: "yasin",
        Produktbild: newOrder.Produktbild,
        Adresse: newOrder.Adresse,
      })
      .then((push) => {
        const imageupload = firebase
          .storage()
          .ref()
          .child("/" + push.getKey())
          .put(files.File)
          .then((snapshot) => {
            snapshot.ref.getDownloadURL().then((downloadURL) => {
              firebase
                .database()
                .ref("serviceobjekte/" + push.getKey())
                .update({
                  ObjectID: push.getKey(),
                  Produktbild: downloadURL,
                });
            });
          });
      })
      .catch((err) => {
        console.log(err);
      });
    handleClose();
  };

  const classes = useStyles();
  console.log(props);
  return (
    <div>
      <Grid container direction="row" justify="center" alignItems="center">
        <Grid item xs={12} style={{ marginBottom: 20 }}>
          {files.File ? (
            <>
              <img
                style={{
                  maxHeight: 200,
                  display: "block",
                }}
                src={files.File ? URL.createObjectURL(files.File) : null}
              />
              <button
                className={classes.buttons2}
                style={{ marginTop: 15 }}
                onClick={() => setFiles({})}
              >
                Bild löschen
              </button>
            </>
          ) : (
            <>
              <input
                // label="Bild hinzufügen"
                id="upload"
                capture="camera"
                type="file"
                className={classes.input}
                onChange={(file) =>
                  setFiles({
                    File: file.target.files[0],
                  })
                }
              />
              <label for={`upload`} className={classes.buttons2}>
                Bild hochladen
              </label>
            </>
          )}
        </Grid>
        <Grid item xs={6} className={classes.gridItem}>
          <InputLabel htmlFor="artikelcode-simple" className={classes.uilabel}>
            Objektkategorie*
          </InputLabel>
          <Select
            disableUnderline
            className={classes.uiinput}
            defaultValue={""}
            value={Objektkategorie}
            onChange={(e) => setObjektKategorie(e.target.value)}
            label={"Artikelcode*"}
            inputProps={{
              name: "Artikelcode",
              id: "artikelcode-simple",
            }}
          >
            <MenuItem value="">
              <em>-</em>
            </MenuItem>
            <MenuItem value="Bank">
              <p>Bank</p>
            </MenuItem>
            <MenuItem value="Haltestelle">
              <p>Haltestelle</p>
            </MenuItem>
            <MenuItem value="Leuchte">
              <p>Leuchte</p>
            </MenuItem>
          </Select>
        </Grid>
        <Grid item xs={6} className={classes.gridItem}>
          <InputLabel htmlFor="Lieferdatum-simple" className={classes.uilabel}>
            Kaufdatum*
          </InputLabel>
          <KeyboardDatePicker
            clearable
            InputProps={{
              disableUnderline: true,
            }}
            className={classes.uiinput}
            value={Kaufdatum}
            onChange={(date) => setKaufdatum(moment(date).format())}
            minDate={new Date()}
            format="DD-MM-YYYY"
          />
        </Grid>
        <Grid item xs={6} className={classes.gridItem}>
          <InputLabel
            htmlFor="Artikelnummer-simple"
            className={classes.uilabel}
          >
            Latitude*
          </InputLabel>
          <input
            className={classes.uiinput}
            required
            type="number"
            id="Artikelnummer-required"
            value={Lat}
            onChange={(e) => setLat(e.target.value)}
          />
        </Grid>
        <Grid item xs={6} className={classes.gridItem}>
          <InputLabel htmlFor="Beschreibung-simple" className={classes.uilabel}>
            Longitude*
          </InputLabel>
          <input
            className={classes.uiinput}
            required
            type={"number"}
            id="Beschreibung-required"
            value={Long}
            onChange={(e) => setLong(e.target.value)}
          />
        </Grid>
        <Grid item xs={6} className={classes.gridItem}>
          <InputLabel htmlFor="Menge-simple" className={classes.uilabel}>
            Adresse*
          </InputLabel>
          <input
            className={classes.uiinput}
            id="menge-number"
            type="text"
            value={Adresse}
            onChange={(e) => setAdresse(e.target.value)}
          />
        </Grid>

        <Grid item xs={6} align="right" className={classes.gridItem}>
          <Button
            className={classes.buttons1}
            variant="contained"
            color="primary"
            onClick={() => handleSubmit(newOrder)}
          >
            Anlegen
          </Button>
          <Button
            className={classes.buttons2}
            variant="contained"
            color="primary"
            autoFocus
            onClick={() => handleClose()}
          >
            Abbrechen
          </Button>
        </Grid>
      </Grid>
    </div>
  );
};

export default AddObjectTable;
