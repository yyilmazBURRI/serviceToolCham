import React, { useState } from "react";

import ReactMapGL, { Marker } from "react-map-gl";
import LocationOnIcon from "@material-ui/icons/LocationOn";

const Map = (props) => {
  const [viewport, setViewport] = useState({
    width: "100%",
    height: "80%",
    latitude: 47.17829041524882,
    longitude: 8.46158514871517,
    zoom: 15,
  });

  return (
    <ReactMapGL
      mapboxApiAccessToken={
        "pk.eyJ1IjoiYnVycmkiLCJhIjoiY2tuc2loMWs4MmkydzJxbnhidmFkYmRtYyJ9.OM79ZdZ8JN6JY7j30TQ9aw"
      }
      {...viewport}
      onViewportChange={(nextViewport) => setViewport(nextViewport)}
      mapStyle={"mapbox://styles/mapbox/basic-v9"}
    >
      {props.wartungen !== undefined
        ? props.wartungen.map((wartung) => (
            <Marker latitude={wartung.Latitude} longitude={wartung.Longitude}>
              <LocationOnIcon />
              {"Das Objekt " + wartung.ObjektID + " hat einen Task offen"}
            </Marker>
          ))
        : null}
    </ReactMapGL>
  );
};

export default Map;
