import React, { useState } from "react";

import ReactMapGL, { Marker } from "react-map-gl";
import LocationOnIcon from "@material-ui/icons/LocationOn";

const Map = (props) => {
  const filteredIncidents = props.incidents.filter(
    (incident) => incident.Status === "Offen"
  );
  const [viewport, setViewport] = useState({
    width: "100%",
    height: "100%",
    latitude: 47.17829041524882,
    longitude: 8.46158514871517,
    zoom: 15,
  });

  return (
    <ReactMapGL
      mapboxApiAccessToken={
        "pk.eyJ1IjoiYnVycmkiLCJhIjoiY2tuc2loMWs4MmkydzJxbnhidmFkYmRtYyJ9.OM79ZdZ8JN6JY7j30TQ9aw"
      }
      {...viewport}
      onViewportChange={(nextViewport) => setViewport(nextViewport)}
      mapStyle={"mapbox://styles/mapbox/basic-v9"}
    >
      {filteredIncidents.map((incident) => (
        <Marker latitude={incident.Latitude} longitude={incident.Longitude}>
          <LocationOnIcon />
          {"Das Objekt " + incident.ObjectID + " hat ein Problem"}
        </Marker>
      ))}
    </ReactMapGL>
  );
};

export default Map;
