import React from "react";
import "./App.css";
import firebase from "firebase/app";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";
import { CookiesProvider } from "react-cookie";

import mapboxgl from "mapbox-gl";

import PublicView from "./frontend/pages/publicview/publicview";
import AdminView from "./frontend/pages/adminview/adminView";

function App() {
  mapboxgl.workerClass =
    // eslint-disable-next-line import/no-webpack-loader-syntax
    require("worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker").default;

  const firebaseConfig = {
    apiKey: "AIzaSyDDfJjH6XIZRW9yep8xPUqrXuTeO51RvgA",
    authDomain: "service-tool-5496d.firebaseapp.com",
    databaseURL:
      "https://service-tool-5496d-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "service-tool-5496d",
    storageBucket: "service-tool-5496d.appspot.com",
    messagingSenderId: "259073289461",
    appId: "1:259073289461:web:bf51b93a8ec5d6c53d69bc",
  };

  !firebase.apps.length
    ? firebase.initializeApp(firebaseConfig)
    : firebase.app();

  return (
    <div>
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <Router>
          <Switch>
            <CookiesProvider>
              <Route exact path="/:objectid" component={PublicView} />
              <Route path="/admin" component={AdminView} />
            </CookiesProvider>
          </Switch>
        </Router>
      </MuiPickersUtilsProvider>
    </div>
  );
}

export default App;
